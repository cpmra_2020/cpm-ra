# cpm-ar

Este repositorio pertenece a la aplicación web de realidad aumentada en desarrollo para la Comisión Provincial por la Memoria por parte de LAIT.

## Sobre el repositorio

La aplicación web está desarrollada con

- [jsartoolkit5](https://github.com/artoolkitx/jsartoolkit5)
- [three.js](https://threejs.org/)

Todas las dependencias a excepción a jsartoolkit5 (incluida en `static/js`) son administradas con [npm](https://www.npmjs.com/)

## Desarrollo y producción

Para compilar la aplicación (tanto en desarrollo como en producción), se utiliza [webpack](https://webpack.js.org/).

Antes de comenzar, se deben instalar las dependencias:

```sh
npm install
```

Para lanzar el servidor de desarrollo (luego de generar los [certificados SSL autofirmados](#ssl)) se debe utilizar el siguiente comando:

```sh
npm run start
```

Para compilar una versión de producción, se utiliza el siguiente comando:

```sh
npm run build
```

## Estilo del código

Para garantizar la consistencia de estilo y calidad del código, se utiliza [eslint](https://eslint.org/) y [prettier](https://prettier.io/). Para consultar por errores y advertencias, se puede utilizar el siguiente comando:

```sh
npm run lint
```

Para arreglar esos errores y advertencias automáticamente (en la medida que sea posible), se debe utilizar el siguiente comando:

```sh
npm run fix
```

## SSL

Para poder hacer uso de la API [MediaDevices](https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices), el navegador debe estar accediendo al un servidor mediante una [conexión segura](https://es.wikipedia.org/wiki/Transport_Layer_Security). El servidor de desarrollo de webpack es capaz de ofrecer una conexión HTTPS, pero para esto se debe proveer un certificado SSL.

Para generar los certificados, se recomienda usar [mkcert](https://github.com/FiloSottile/mkcert) de la siguiente manera:

```sh
mkcert -key-file ssl/key.pem -cert-file ssl/cert.pem localhost 0.0.0.0 127.0.0.1 "IP PRIVADA"
```

(Reemplazar `"IP PRIVADA"` por dirección de IP privada o hostname para poder acceder al servidor desde la red)

Para instalar usar los certificados en dispositivos moviles, se debe instalar el certificado de la autoridad de certificados (`rootCA.pem`), que se puede encontrar en el directorio listado por

```sh
mkcert -CAROOT
```

## Desarrollando en Android sin SSL

Se puede hacer uso de [ADB](https://developer.android.com/studio/command-line/adb) para abrir un tunel y acceder al servidor a través de este

```sh
adb connect
# adb connect <ip privada> # En el caso de usar adb over network
adb reverse tcp:8080 tcp:8080
```

Si el servidor está abierto, se puede acceder a este a través del navegador del celular en [http://localhost:8080]()
