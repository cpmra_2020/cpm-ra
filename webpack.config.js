const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const absolutePaths = ['ssl', 'ssl/key.pem', 'ssl/cert.pem'].map((p) => path.join(__dirname, p));
const httpsPresent = () => absolutePaths.every((p) => fs.existsSync(p));
const https = httpsPresent()
  ? {
    key: absolutePaths[1],
    cert: absolutePaths[2],
  } : undefined;

// eslint-disable-next-line no-console
if (!https) console.warn('HTTPS not configured, falling back to HTTP');

module.exports = (env) => ({
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: (env && env.ASSET_PATH) || '/',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      },
    ],
  },
  devtool: 'source-map',
  devServer: {
    host: '0.0.0.0',
    port: 8080,
    https,
    contentBase: path.join(__dirname, 'static'),
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: './index.html',
    }),
    new CopyWebpackPlugin([{ from: 'static', to: '.' }]),
  ],
});
