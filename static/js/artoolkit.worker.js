importScripts('./artoolkit.min.js');

self.onmessage = function(e) {
  const msg = e.data;
  switch (msg.type) {
    case "load": {
      load(msg);
      return;
    }
    case "process": {
      next = msg.imagedata;
      process();
      return;
    }
  }
};

let next = null;

let ar = null;
let markerResult = null;

function load(msg) {
  const param = new ARCameraParam(`../${msg.cameraParameters}`);

  param.onload = function() {
    ar = new ARController(msg.canvasWidth, msg.canvasHeight, param);
    const cameraMatrix = ar.getCameraMatrix();

    const markerUrls = msg.markers.map((marker) => `../${marker.url}`)
    const markerIds = msg.markers.map((marker) => marker.id)

    ar.addEventListener('getNFTMarker', function(ev) {
      markerResult = { type: "found", marker: markerIds[ev.data.index], matrixGL_RH: JSON.stringify(ev.data.matrixGL_RH), proj: JSON.stringify(cameraMatrix) };
    });

    ar.loadNFTMarkers(markerUrls, function(markerIds) {
      ar.trackNFTMarkerId(markerIds, 2)
      postMessage({ type: "endLoading", end: true })
    },
      function(error) {
        postMessage({ type: "error", error: true })
        console.error(error);
      });

    postMessage({ type: "loaded", proj: JSON.stringify(cameraMatrix) });
  };
}

function process() {

  markerResult = null;

  if (ar) {
    ar.process(next);
  }

  if (markerResult) {
    postMessage(markerResult);
  } else {
    postMessage({ type: "not found" });
  }

  next = null;
}
