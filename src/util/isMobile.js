function isMobile() {
  return /Android|mobile|iPad|iPhone/i.test(navigator.userAgent)
}

export default isMobile
