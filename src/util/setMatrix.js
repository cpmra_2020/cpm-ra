function setMatrix(source, destination) {
  const dest = destination

  const array = Object.values(source)

  if (typeof dest.elements.set === 'function') {
    dest.elements.set(array)
  } else {
    dest.elements = [].slice.call(array)
  }
}

export default setMatrix
