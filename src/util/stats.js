import Stats from 'stats.js'

// Stats.showPanel() parameters:
// 0: fps, 1: ms, 2: mb, 3+: custom

const statsMain = new Stats()
statsMain.showPanel(0)
// document.getElementById('stats1').appendChild(statsMain.dom);

const statsWorker = new Stats()
statsWorker.showPanel(0)
// document.getElementById('stats2').appendChild(statsWorker.dom);

export { statsMain, statsWorker }
