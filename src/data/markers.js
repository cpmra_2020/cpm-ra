const markers = {
  pinball: {
    id: 'pinball',
    width: 1637,
    height: 2048,
    dpi: 250,
    url: 'data/NFT/pinball',
  },
  V0c: {
    id: 'V0c',
    width: 2048,
    height: 2048,
    dpi: '?',
    url: 'data/NFT/V0-c',
  },
}

export default markers
