import markers from './markers'

export const DISAPPEAR_TIMEOUT = 5

export const EVENTS = {
  show: 'show',
  beginDisappear: 'beginDisappear',
  endDisappear: 'endDisappear',
}

export const SCENES = {
  testScene: {
    marker: markers.V0c,
    modelsData: [
      {
        modelURI: 'data/models/test.glb',
        modelPosition: [100, 0, -200],
        modelScale: [200, 200, 200],
        modelRotation: [0, 0, 0],
      },
    ],
    envMapData: {
      id: 'venice-sunset',
      name: 'Venice Sunset',
      path: 'img/venice_sunset_1k.hdr',
      format: '.hdr',
    },
  },
  testScene2: {
    marker: markers.pinball,
    modelsData: [
      {
        modelURI: 'data/models/character1.glb',
        modelPosition: [100, 0, -50],
        modelScale: [500, 500, 500],
        modelRotation: [0, 0, 0],
      },
      {
        modelURI: 'data/models/character2.glb',
        modelPosition: [100, 0, -50],
        modelScale: [500, 500, 500],
        modelRotation: [0, 0, 0],
      },
    ],
    envMapData: {
      id: 'venice-sunset',
      name: 'Venice Sunset',
      path: 'img/venice_sunset_1k.hdr',
      format: '.hdr',
    },
  },
}
