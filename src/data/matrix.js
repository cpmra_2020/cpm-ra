export const interpolationFactor = 24

const trackingMatrix = {
  // for interpolation
  delta: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  interpolated: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
}

export function createMatrix() {
  return { ...trackingMatrix }
}
