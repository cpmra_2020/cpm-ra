import { setCameraMatrix } from '../three/camera'
import { canvasHeight, canvasWidth, h, processVideo, w } from './processVideo'

import { statsWorker } from '../util/stats'

import { NFT_LOADING } from '../data/loadingFlags'
import { NFT_ERROR } from '../data/errorFlags'
import store from '../store'

const cameraParameters = 'data/camera_para-iPhone5.dat'

function found(msg) {
  if (!msg) {
    return null
  }

  const foundMarker = JSON.parse(msg.matrixGL_RH)
  return { marker: msg.marker, matrix: foundMarker }
}

function buildWorker(cb, markers) {
  const worker = new Worker('./js/artoolkit.worker.js')
  store.setLoading(NFT_LOADING)
  worker.postMessage({
    type: 'load',
    canvasWidth,
    canvasHeight,
    cameraParameters,
    markers: markers,
  })

  worker.onmessage = (ev) => {
    const msg = ev.data
    switch (msg.type) {
      case 'loaded': {
        setCameraMatrix(msg.proj, canvasWidth, canvasHeight, w, h)
        break
      }
      case 'error': {
        store.setError(NFT_ERROR)
        break
      }
      case 'endLoading': {
        if (msg.end === true) {
          store.removeLoading(NFT_LOADING)
        }
        break
      }
      case 'found': {
        cb(found(msg))
        break
      }
      case 'not found': {
        cb(found(null))
        break
      }
      default: {
        // eslint-disable-next-line no-console
        console.warn(`Unexpected message type: ${msg.type}`)
      }
    }

    statsWorker.update()

    const imageData = processVideo()
    if (imageData) {
      worker.postMessage({ type: 'process', imagedata: imageData }, [
        imageData.data.buffer,
      ])
    }
  }
  return worker
}

export default buildWorker
