const canvasProcess = document.createElement('canvas')
const contextProcess = canvasProcess.getContext('2d')

const scaleFactor = 320 / Math.max(480, (640 / 3) * 4)
const w = 480 * scaleFactor
const h = 640 * scaleFactor
const canvasWidth = Math.max(w, (h / 3) * 4)
const canvasHeight = Math.max(h, (w / 4) * 3)
const ox = (canvasWidth - w) / 2
const oy = (canvasHeight - h) / 2

canvasProcess.style.clientWidth = `${canvasWidth}px`
canvasProcess.style.clientHeight = `${canvasHeight}px`

// Debug
//
// canvasProcess.style.position = `absolute`;
// canvasProcess.style.zIndex = 100000;
// canvasProcess.style.top = 0;
//
// canvasProcess.width = canvasWidth;
// canvasProcess.height = canvasHeight;
//
// document.body.appendChild(canvasProcess);

function processVideo() {
  const video = document.getElementById('video')

  if (!video) {
    return
  }

  contextProcess.fillStyle = 'black'
  contextProcess.fillRect(0, 0, canvasWidth, canvasHeight)
  contextProcess.drawImage(video, 0, 0, video.videoWidth, video.videoHeight, ox, oy, w, h)

  const imageData = contextProcess.getImageData(0, 0, canvasWidth, canvasHeight)
  return imageData
}

export { processVideo, canvasWidth, canvasHeight, w, h }
