import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
  }

   #app {
    height: 100%
  }

  html, body {
    overflow: hidden;
    margin: 0;
    padding: 0;
    width: ${(props) => props.width}px;
    height: ${(props) => props.height}px;

    background: black;
    color: white;
    font-family: sans-serif;
    font-feature-settings: "lnum"; 
  }
`

export default GlobalStyle
