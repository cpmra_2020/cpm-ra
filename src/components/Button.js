import styled from 'styled-components'

const Button = styled.button`
  width: 85%;
  padding: 5% 0;
  align-self: ${(props) => props.alignSelf || 'inherit'};
  margin: 2vh;
  color: white;
  background: black;
  border: 2px solid white;
`

export default Button
