import React from 'react'
import styled from 'styled-components'
import { motion } from 'framer-motion'

import WarningIcon from '@material-ui/icons/Warning'

const Container = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  width: 100%;
  align-self: flex-end;
  z-index: 1000;
  margin: 6px;
  width: 72px;
  height: 72px;
  bottom: 0;
`

function TrackingLostIndicator() {
  return (
    <Container
      animate={{
        opacity: [0, 1, 0],
      }}
      transition={{
        ease: 'easeInOut',
        duration: 2,
        loop: Infinity,
      }}
    >
      <WarningIcon fontSize="large" style={{ color: '#ffde03' }} />
    </Container>
  )
}

export default TrackingLostIndicator
