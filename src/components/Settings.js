import React from 'react'
import { PropTypes } from 'prop-types'
import { observer } from 'mobx-react'

import CloseIcon from '@material-ui/icons/Close'

import Text from './Text'
import TopBar from './TopBar'
import IconButton from './IconButton'

@observer
class Settings extends React.Component {
  render() {
    const { store, handleCloseButton } = this.props
    const { debug, renderHD } = store

    return (
      <>
        <TopBar>
          <Text>Ajustes</Text>
          <div style={{ margin: '0 auto' }} />
          <IconButton
            cb={handleCloseButton}
            icon={<CloseIcon fontSize="large" style={{ color: 'white' }} />}
          />
        </TopBar>

        <div style={{ marginBottom: '24px' }}>
          <label htmlFor="renderHD">
            <input
              type="checkbox"
              checked={renderHD}
              onChange={() => {
                store.toggleRenderHD()
              }}
              id="renderHD"
            />
            Mostrar gráficos 3D en alta definición
          </label>

          <br />

          <label htmlFor="debug">
            <input
              type="checkbox"
              checked={debug}
              onChange={() => {
                store.toggleDebug()
              }}
              id="debug"
            />
            Mostrar estadísticas de rendimiento
          </label>
        </div>
      </>
    )
  }
}

Settings.propTypes = {
  handleCloseButton: PropTypes.func.isRequired,
  store: PropTypes.shape({
    debug: PropTypes.bool.isRequired,
    toggleDebug: PropTypes.func.isRequired,
    renderHD: PropTypes.bool.isRequired,
    toggleRenderHD: PropTypes.func.isRequired,
  }).isRequired,
}

export default Settings
