import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Button = styled.button`
  margin: 6px;
  width: 72px;
  height: 72px;
  background-color: rgba(0, 0, 0, 0);
  border: 0;
`

function IconButton(props) {
  const { icon, cb } = props

  return (
    <Button type="button" onClick={cb}>
      {icon}
    </Button>
  )
}

IconButton.propTypes = {
  icon: PropTypes.element.isRequired,
  cb: PropTypes.func.isRequired,
}

export default IconButton
