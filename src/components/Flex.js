import styled from 'styled-components'

const Flex = styled.div`
  position: absolute;
  display: flex;
  flex-direction: ${(props) => props.direction || 'row'};
  flex-wrap: ${(props) => props.wrap || 'wrap'};
  justify-content: ${(props) => props.justifyContent || 'center'};
  align-content: ${(props) => props.alignContent || 'space-between'};
  align-items: ${(props) => props.alignItems || 'center'};
  height: 100%;
  width: 100%;
  background: rgba(0, 0, 0, 0.6);
  z-index: ${(props) => props.zIndex || 99};
`

export default Flex
