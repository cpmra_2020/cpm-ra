import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StatsDiv = styled.div`
  padding: 0.25rem;
  width: 5.5rem;
  margin: 0.5rem;
  background: rgba(255, 255, 255, 0.5);
  border-radius: 6px;
`

const StatsTitle = styled.p`
  margin: 0 0 0.25rem 0;
  font-size: 0.75rem;
`

class StatsItem extends React.PureComponent {
  constructor(props) {
    super(props)

    this.ref = React.createRef()
  }

  componentDidMount() {
    const { stats } = this.props
    const { dom } = stats

    this.ref.current.appendChild(dom)
  }

  render() {
    const { title } = this.props

    const Title = () => {
      if (title) {
        return <StatsTitle>{title}</StatsTitle>
      }

      return null
    }

    return <StatsDiv ref={this.ref}>{Title()}</StatsDiv>
  }
}

StatsItem.propTypes = {
  title: PropTypes.string,
  stats: PropTypes.shape({
    dom: PropTypes.object.isRequired,
  }).isRequired,
}

StatsItem.defaultProps = {
  title: undefined,
}

export default StatsItem
