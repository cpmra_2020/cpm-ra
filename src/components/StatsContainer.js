import styled from 'styled-components'

const StatsContainer = styled.div`
  position: absolute;
  z-index: 10000;
  width 50px;
  & div {
    position: relative !important;
    }
`

export default StatsContainer
