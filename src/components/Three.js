import React from 'react'
import styled from 'styled-components'
import { Clock } from 'three'

import renderer from '../three/renderer'
import { camera } from '../three/camera'

import store from '../store'

import Scene from '../three/Scene'

import buildWorker from '../ar/worker'

import { statsMain } from '../util/stats'

import { SCENES } from '../data/scene'

const Container = styled.div`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0);
`

class Three extends React.PureComponent {
  constructor(props) {
    super(props)

    this.ref = React.createRef()
    this.worker = undefined
    this.animate = this.animate.bind(this)
    this.processAR = this.processAR.bind(this)
  }

  componentDidMount() {
    this.ref.current.appendChild(renderer.domElement)

    this.clock = new Clock()

    this.scenes = [new Scene(SCENES.testScene), new Scene(SCENES.testScene2)]
    this.worker = buildWorker(
      this.processAR,
      this.scenes.map((s) => s.getMarker()),
    )

    this.animate()
  }

  processAR(workerMsg) {
    if (store.currentScene) {
      if (workerMsg) {
        store.currentScene.updateAR(workerMsg.matrix)
        return
      }

      store.currentScene.updateAR(null)
      return
    }

    if (!workerMsg) return

    this.scenes.some((scene) => {
      if (workerMsg.marker === scene.getId()) {
        store.setCurrentScene(scene)
        return true
      }
      return false
    })

    store.currentScene.updateAR(workerMsg.matrix)
  }

  animate() {
    requestAnimationFrame(this.animate)
    statsMain.update()
    const deltaTime = this.clock.getDelta()

    renderer.clear()

    const { currentScene } = store

    if (currentScene) {
      currentScene.updateAnimationMixers(deltaTime)
      renderer.render(currentScene.scene, camera)
    }
  }

  render() {
    return <Container id="three" ref={this.ref} />
  }
}

export default Three
