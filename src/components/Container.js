import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Wrapper = styled.div`
  position: relative;
  width: ${(props) => props.width}px;
  height: 100%;

  margin: ${(props) => (props.centered ? '0 auto' : 0)};
  padding: 0;

  overflow: hidden;

  font-size: 3vh;

  transform: translateX(${(props) => props.translateX || 0}px);
  background: rgba(0, 0, 0, 0.5);
`

const Container = ({ ratio, centered, show, children }) => {
  if (!show) {
    return null
  }

  const [w, h] = ratio.split(':')
  const factor = w / h

  const width = window.innerHeight * factor

  const excess = window.innerWidth - window.innerHeight * factor
  let translateValue = 0

  if (excess < 0) {
    translateValue = excess / 2
  }

  return (
    <Wrapper width={width} centered={centered} translateX={translateValue}>
      {children}
    </Wrapper>
  )
}

Container.propTypes = {
  ratio: PropTypes.string,
  centered: PropTypes.bool,
  show: PropTypes.bool,
  children: PropTypes.node.isRequired,
}

Container.defaultProps = {
  centered: true,
  show: true,
  ratio: '9:16',
}

export default Container
