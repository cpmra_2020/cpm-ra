import React from 'react'
import { PropTypes } from 'prop-types'
import { observer } from 'mobx-react'

import Flex from './Flex'
import Text from './Text'

@observer
class Error extends React.PureComponent {
  render() {
    const { store } = this.props
    const { errorFlags } = store

    const errors = () =>
      errorFlags.map((e) => (
        <Text key={e} opacity="0.25">
          {e}
        </Text>
      ))

    return (
      <Flex>
        <Text>
          ¡Ocurrió un error!
          <br />
          Intentá recargar la página.
        </Text>
        <br />
        {errors()}
      </Flex>
    )
  }
}

Error.propTypes = {
  store: PropTypes.shape({
    errorFlags: PropTypes.array.isRequired,
  }).isRequired,
}

export default Error
