import React from 'react'
import { PropTypes } from 'prop-types'
import { observer } from 'mobx-react'

import { Ring } from 'react-awesome-spinners'

import Flex from './Flex'
import Text from './Text'

@observer
class Loading extends React.PureComponent {
  render() {
    const { store } = this.props
    const { loadingFlags } = store

    if (loadingFlags.length === 0) {
      return null
    }

    return (
      <Flex direction="column" alignContent="center">
        <div
          style={{
            alignSelf: 'center',
            textAlign: 'center',
          }}
        >
          <Ring />
          <Text>Cargando</Text>
        </div>
      </Flex>
    )
  }
}

Loading.propTypes = {
  store: PropTypes.shape({
    loadingFlags: PropTypes.array.isRequired,
  }).isRequired,
}

export default Loading
