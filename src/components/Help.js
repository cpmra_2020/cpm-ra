import React from 'react'
import { PropTypes } from 'prop-types'
import { observer } from 'mobx-react'

import SettingsIcon from '@material-ui/icons/Settings'

import Settings from './Settings'

import Flex from './Flex'
import Text from './Text'
import Button from './Button'
import TopBar from './TopBar'
import IconButton from './IconButton'

const helpText = `
Busca marcadores en el edificio y apuntales con tu celular
`

@observer
class Help extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      showSettings: false,
    }

    this.handleClick = this.handleClick.bind(this)
  }

  handleClick() {
    this.setState((state) => ({
      showSettings: !state.showSettings,
    }))
  }

  render() {
    const { store } = this.props
    const { showHelp, loadingFlags } = store

    if (!showHelp || loadingFlags.length !== 0) {
      return null
    }

    const { showSettings } = this.state

    const content = () => {
      if (showSettings) {
        return <Settings store={store} handleCloseButton={this.handleClick} />
      }

      return (
        <>
          <TopBar>
            <div style={{ margin: '0 auto' }} />
            <IconButton
              cb={this.handleClick}
              icon={<SettingsIcon fontSize="large" style={{ color: 'white' }} />}
            />
          </TopBar>
          <Text>{helpText}</Text>
          <Button
            alignSelf="flex-end"
            onClick={() => {
              store.toggleHelp()
            }}
          >
            ¡Entendido!
          </Button>
        </>
      )
    }

    return <Flex>{content()}</Flex>
  }
}

Help.propTypes = {
  store: PropTypes.shape({
    loadingFlags: PropTypes.array.isRequired,
    showHelp: PropTypes.bool.isRequired,
    toggleHelp: PropTypes.func.isRequired,
  }).isRequired,
}

export default Help
