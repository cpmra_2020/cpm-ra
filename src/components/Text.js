import styled from 'styled-components'

const Text = styled.p`
  color: white;
  text-align: center;
  opacity: ${(props) => props.opacity || 1};
  align-self: ${(props) => props.alignSelf || 'inherit'};
  padding: 12px 24px;
`

export default Text
