import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background: rgba(0, 0, 0, 0);
  position: ${(props) => props.position || 'relative'};
  top: 0;
  z-index: 10;
`

function TopBar(props) {
  const { children, position } = props

  return <Container position={position}>{children}</Container>
}

TopBar.propTypes = {
  children: PropTypes.node.isRequired,
  position: PropTypes.string,
}

TopBar.defaultProps = {
  position: undefined,
}

export default TopBar
