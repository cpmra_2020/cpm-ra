import React from 'react'
import styled from 'styled-components'

import { VIDEO_LOADING } from '../data/loadingFlags'
import { VIDEO_ERROR } from '../data/errorFlags'
import store from '../store'

const VideoElement = styled.video`
  position: absolute;
  z-index: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`

class Video extends React.Component {
  constructor(props) {
    super(props)

    this.ref = React.createRef()
  }

  componentDidMount() {
    this.ref.current.setAttribute('muted', '1')

    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      const hint = {
        audio: false,
        video: {
          width: { ideal: 640 },
          height: { ideal: 480 },
          facingMode: 'environment',
        },
      }

      navigator.mediaDevices.getUserMedia(hint).then(
        (stream) => {
          if (this.ref.current) {
            store.setLoading(VIDEO_LOADING)
            this.ref.current.srcObject = stream
            this.ref.current.addEventListener('loadedmetadata', () => {
              this.ref.current.play()
              store.removeLoading(VIDEO_LOADING)
              // start worker?
            })
          }
        },
        () => {
          store.setError(VIDEO_ERROR)
        },
      )
    } else {
      store.setError(VIDEO_ERROR)
    }
  }

  render() {
    return <VideoElement autoPlay playsInline id="video" ref={this.ref} />
  }
}

export default Video
