import { observable } from 'mobx'
import { find, remove } from 'lodash'

import renderer from './three/renderer'

const dev = process.env.NODE_ENV === 'development'

class Store {
  @observable loadingFlags = []

  @observable errorFlags = []

  @observable showHelp = true

  @observable debug = dev

  @observable renderHD = true

  @observable currentScene = null
  @observable trackingLost = false

  setLoading(loader) {
    this.loadingFlags.push(loader)
  }

  removeLoading(loader) {
    remove(this.loadingFlags, (l) => l === loader)
  }

  setError(error) {
    if (!find(this.errorFlags, (e) => e === error)) {
      this.errorFlags.push(error)
    }
  }

  toggleHelp() {
    this.showHelp = !this.showHelp
  }

  toggleDebug() {
    this.debug = !this.debug
  }

  toggleRenderHD() {
    if (renderer.getPixelRatio() === window.devicePixelRatio) {
      this.renderHD = false
      renderer.setPixelRatio(window.devicePixelRatio / 2)
    } else {
      this.renderHD = true
      renderer.setPixelRatio(window.devicePixelRatio)
    }
  }

  setTrackingLost(value) {
    if (value === false || value === true) {
      this.trackingLost = value
    }
  }

  setCurrentScene(id) {
    this.currentScene = id
  }
}

const store = new Store()

export default store
