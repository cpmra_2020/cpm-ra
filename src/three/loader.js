import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import store from '../store'

import { SCENE_ERROR } from '../data/errorFlags'

const loader = new GLTFLoader()

function loadGLTF(modelURI) {
  return new Promise((resolve, reject) => {
    loader.load(
      modelURI,
      (gltf) => {
        resolve(gltf)
      },
      undefined,
      (error) => {
        store.setError(SCENE_ERROR)
        //console.error(`Couldn't get model: ${modelURI}`)
        reject(error)
      },
    )
  })
}

export { loader, loadGLTF }
