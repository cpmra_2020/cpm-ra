import { WebGLRenderer, sRGBEncoding } from 'three'

const renderer = new WebGLRenderer({
  alpha: true,
  antialias: true,
  preserveDrawingBuffer: true,
})

renderer.outputEncoding = sRGBEncoding
renderer.setSize(window.innerWidth, window.innerHeight)
renderer.setPixelRatio(window.devicePixelRatio)
renderer.autoClear = false

export default renderer
