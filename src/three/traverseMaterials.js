function traverseMaterials(object, callback) {
  object.traverse((node) => {
    if (!node.isMesh) return
    const materials = Array.isArray(node.material) ? node.material : [node.material]
    materials.forEach(callback)
  })
}

export default traverseMaterials
