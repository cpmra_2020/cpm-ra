import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader'

import { UnsignedByteType, PMREMGenerator } from 'three'

import renderer from './renderer'

const pmremGenerator = new PMREMGenerator(renderer)
pmremGenerator.compileEquirectangularShader()

function getCubeMapTexture(environment) {
  const { path } = environment

  // no envmap
  if (!path) return Promise.resolve({ envMap: null })

  return new Promise((resolve, reject) => {
    new RGBELoader().setDataType(UnsignedByteType).load(
      path,
      (texture) => {
        const envMap = pmremGenerator.fromEquirectangular(texture).texture
        pmremGenerator.dispose()

        resolve({ envMap })
      },
      undefined,
      reject,
    )
  })
}

export default getCubeMapTexture
