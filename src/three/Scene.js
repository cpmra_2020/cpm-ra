import { Scene as ThreeScene, Object3D } from 'three'

import { EVENTS, DISAPPEAR_TIMEOUT } from '../data/scene'

import { camera } from './camera'
import getCubeMapTexture from './cubeMapTexture'
import { createMatrix, interpolationFactor } from '../data/matrix'
import setMatrix from '../util/setMatrix'

import { SCENE_LOADING } from '../data/loadingFlags'
import { SCENE_ERROR } from '../data/errorFlags'
import store from '../store'

import Model from './Model'

class Scene {
  constructor(resources, init = true) {
    this.resources = resources

    const { marker } = resources

    this.id = marker.id
    this.marker = marker

    this.scene = new ThreeScene()
    this.scene.add(camera)

    this.envMap = undefined
    this.models = []

    this.root = new Object3D()
    this.root.matrixAutoUpdate = false
    this.root.visible = false
    this.scene.add(this.root)

    this.matrix = createMatrix()

    this.disappearTimeout = null

    this.root.addEventListener(EVENTS.show, () => {
      this.root.visible = true

      if (this.disappearTimeout) {
        clearTimeout(this.disappearTimeout)
        this.disappearTimeout = null

        this.models.forEach((model) => {
          model.setOpacity(1)
          model.resumeAnimation()
        })
      }
    })

    this.root.addEventListener(EVENTS.beginDisappear, () => {
      if (!this.disappearTimeout) {
        this.disappearTimeout = setTimeout(() => {
          this.root.dispatchEvent({ type: EVENTS.endDisappear })
        }, DISAPPEAR_TIMEOUT * 1000)

        this.models.forEach((model) => {
          model.setOpacity(0.5)
          model.pauseAnimation()
        })
      }
    })

    this.root.addEventListener(EVENTS.endDisappear, () => {
      store.setTrackingLost(false)
      store.setCurrentScene(null)
      this.root.visible = false
    })

    this.updateAR = this.updateAR.bind(this)
    this.loadResources = this.loadResources.bind(this)
    this.getId = this.getId.bind(this)
    this.getMarker = this.getMarker.bind(this)

    if (init) {
      this.loadResources()
    }
  }

  async loadResources() {
    const load = async () => {
      const { modelsData, envMapData } = this.resources

      // Load model
      modelsData.forEach(async (data) => {
        const model = new Model(data)
        await model.loadModel().then(() => {
          this.models.push(model)
          this.root.add(model.model)
        })
      })

      // Load environment Map
      getCubeMapTexture(envMapData).then(
        ({ envMap }) => {
          this.scene.environment = envMap
        },
        () => {
          store.setError(SCENE_ERROR)
          throw new Error("Couldn't get envMap")
        },
      )
    }

    store.setLoading(SCENE_LOADING)
    await load().then(() => {
      store.removeLoading(SCENE_LOADING)
    })
  }

  updateAnimationMixers(deltaTime) {
    this.models.forEach((model) => {
      if (model.mixer) {
        model.mixer.update(deltaTime)
      }
    })
  }

  getMarker() {
    return this.marker
  }

  getId() {
    return this.id
  }

  updateAR(foundMarkerMatrix) {
    if (!foundMarkerMatrix) {
      this.root.dispatchEvent({ type: EVENTS.beginDisappear })
      if (this.root.visible) {
        store.setTrackingLost(true)
      }
      return
    }

    this.root.dispatchEvent({ type: EVENTS.show })
    store.setTrackingLost(false)

    // interpolate matrix
    for (let i = 0; i < 16; i += 1) {
      this.matrix.delta[i] = foundMarkerMatrix[i] - this.matrix.interpolated[i]
      this.matrix.interpolated[i] += this.matrix.delta[i] / interpolationFactor
    }

    // set matrix of 'root' by detected 'foundMarker' matrix
    setMatrix(this.matrix.interpolated, this.root.matrix)
  }
}

export default Scene
