import { Camera } from 'three'

import setMatrix from '../util/setMatrix'

const camera = new Camera()
camera.matrixAutoUpdate = false

function setCameraMatrix(workerMessage, pw, ph, w, h) {
  // TODO: Simplify. Maybe remove size parameters and import variables
  const proj = JSON.parse(workerMessage)

  const near = 1
  const far = 100000

  const ratioW = pw / w
  const ratioH = ph / h
  proj[0] *= ratioW
  proj[4] *= ratioW
  proj[8] *= ratioW
  proj[12] *= ratioW
  proj[1] *= ratioH
  proj[5] *= ratioH
  proj[9] *= ratioH
  proj[13] *= ratioH
  proj[10] = -(far + near) / (far - near)
  proj[14] = -(2 * far * near) / (far - near)
  setMatrix(proj, camera.projectionMatrix)
}

export { camera, setCameraMatrix }
