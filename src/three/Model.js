import { AnimationMixer, sRGBEncoding } from 'three'

import { loadGLTF } from './loader'
import traverseMaterials from './traverseMaterials'

class Model {
  constructor(resources) {
    this.resources = resources

    this.model = undefined
    this.mixer = undefined
    this.action = undefined
    this.clips = undefined
  }

  async loadModel() {
    const { modelURI, modelPosition, modelScale, modelRotation } = this.resources

    await loadGLTF(modelURI).then((gltf) => {
      const { scene, animations } = gltf

      this.model = scene

      this.mixer = new AnimationMixer(this.model)
      this.clips = animations || []
      if (this.clips.length > 0) {
        this.action = this.mixer.clipAction(this.clips[0])
        this.action.play()
      }

      this.model.position.fromArray(modelPosition)
      this.model.scale.fromArray(modelScale)
      this.model.rotation.fromArray(modelRotation)

      this.setupMaterials()
    })
  }

  setupMaterials() {
    traverseMaterials(this.model, (mat) => {
      const material = mat
      if (material.map) {
        material.map.encoding = sRGBEncoding
      }
      if (material.emissiveMap) {
        material.emissiveMap.encoding = sRGBEncoding
      }
      if (material.map || material.emissiveMap) {
        material.needsUpdate = true
      }
    })
  }

  pauseAnimation() {
    this.action.paused = true
  }

  resumeAnimation() {
    this.action.paused = false
  }

  setOpacity(opacity) {
    traverseMaterials(this.model, (mat) => {
      const material = mat

      material.transparent = false
      material.opacity = opacity
    })
  }
}

export default Model
