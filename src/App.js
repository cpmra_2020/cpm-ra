import React from 'react'
import { PropTypes } from 'prop-types'
import { observer } from 'mobx-react'

import HelpIcon from '@material-ui/icons/Help'

import GlobalStyle from './components/GlobalStyle'
import Container from './components/Container'
import Loading from './components/Loading'
import Error from './components/Error'
import Video from './components/Video'
import Three from './components/Three'
import Help from './components/Help'

import StatsContainer from './components/StatsContainer'
import StatsItem from './components/Stats'

import TopBar from './components/TopBar'
import IconButton from './components/IconButton'
import TrackingLostIndicator from './components/TrackingLostIndicator'

import { statsMain, statsWorker } from './util/stats'

@observer
class App extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      height: window.innerHeight,
      width: window.innerWidth,
    }

    this.setScreenSize = this.setScreenSize.bind(this)
  }

  componentDidMount() {
    window.addEventListener('resize', this.setScreenSize, false)
  }

  setScreenSize() {
    this.setState({
      height: window.innerHeight,
      width: window.innerWidth,
    })
  }

  render() {
    const { height, width } = this.state
    const { store } = this.props
    const { errorFlags, loadingFlags, showHelp, debug, trackingLost } = store

    if (errorFlags.length > 0) {
      return (
        <>
          <GlobalStyle height={height} width={width} />
          <Container>
            <Error store={store} />
          </Container>
        </>
      )
    }

    const stats = () => (
      <StatsContainer>
        <StatsItem title="3D" stats={statsMain} />
        <StatsItem title="AR" stats={statsWorker} />
      </StatsContainer>
    )

    const mainContent = () => {
      if (loadingFlags > 0 || showHelp) {
        return null
      }

      return (
        <>
          {debug ? stats() : null}
          <TopBar position="absolute">
            <div style={{ margin: '0 auto' }} />
            <IconButton
              cb={() => {
                store.toggleHelp()
              }}
              icon={<HelpIcon fontSize="large" style={{ color: 'white' }} />}
            />
          </TopBar>
          {trackingLost ? <TrackingLostIndicator /> : null}
        </>
      )
    }

    return (
      <>
        <GlobalStyle height={height} width={width} />
        <Container>
          {mainContent()}

          <Loading store={store} />
          <Help store={store} />
          <Three />
          <Video />
        </Container>
      </>
    )
  }
}

App.propTypes = {
  store: PropTypes.shape({
    errorFlags: PropTypes.array.isRequired,
    loadingFlags: PropTypes.array.isRequired,
    showHelp: PropTypes.bool.isRequired,
    toggleHelp: PropTypes.func.isRequired,
    debug: PropTypes.bool.isRequired,
    trackingLost: PropTypes.bool.isRequired,
  }).isRequired,
}

export default App
